package dbf.chiquinho.Chiquinho.Sorvetes.web.rest;

import dfb.chiquinho.Chiquinho.Sorvetes.model.User;
import dfb.chiquinho.Chiquinho.Sorvetes.repository.UserRepository;
import org.apache.tomcat.util.http.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/public/")
public class UserResource {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/teste")
    public String teste() {
        return "Olá Mundo";
    }

    @GetMapping("/usuarios")
    public List<User> listaUser() {
        return userRepository.findAll();
    }

    // @GetMapping("/usuarios/{cpf}")
    // public ResponseEntity<User> getUsers(@PathVariable String cpf){
    //     User u = userRepository.findByActiveTrueAndCPF(cpf);

    //     if( u == null){
    //         return ResponseEntity.notFound().build();
    //     }
        
    //     return ResponseEntity.ok().body(u);
    // }

    // @DeleteMapping("/usuarios/{cpf}")
    // public User deleteUser(@PathVariable String cpf){
    //     User u = userRepository.findUserByCpf(cpf);

    //     if( u == null){
    //         return ResponseEntity.notFound().build();
    //     }

    //     u.setActive(false);
    //     return ResponseEntity.ok();
    // }

    @PutMapping("/usuarios")
    public ResponseEntity<User> insert(@RequestBody String userName){
        User u = new User();
        u.setEmail("meu@email.com");
        u.setCpf("151.118.976-29");
        u.setPhone("(31) 98870-5937");
        u.setActive(true);

        u = userRepository.save(u);
        return null;
        // return ResponseEntity.created().body(u);
    }
}
