package dfb.chiquinho.Chiquinho.Sorvetes.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = { "dfb.chiquinho.Chiquinho.Sorvetes.model" })
@EnableJpaRepositories(basePackages = { "dfb.chiquinho.Chiquinho.Sorvetes.repository" })
@ComponentScan(basePackages = { "dfb.chiquinho.Chiquinho.Sorvetes" })
public class ChiquinhoSorvetesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChiquinhoSorvetesApplication.class, args);
	}
	
}
