package dfb.chiquinho.Chiquinho.Sorvetes.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "voucher")
public class Voucher {

    @Id
    @Column(name = "voucher_id")
    private int id;

    @Column(name = "voucherName")
    private String voucherName;

    @Column(name = "description")
    private String description;

    @Column(name = "recieveDate")
    private Date recieveDate;

    @Column(name = "useDate")
    private Date useDate;

    @Column(name = "discount")
    private double discount;

    @Column(name = "active")
    private boolean active;

    @ManyToMany(mappedBy = "vouchers")
    Set<User> users;


}
