package dfb.chiquinho.Chiquinho.Sorvetes.service;

import dfb.chiquinho.Chiquinho.Sorvetes.model.User;
import dfb.chiquinho.Chiquinho.Sorvetes.repository.UserRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserService{
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public User saveUser(User user){
        user.setActive(true);
        return this.userRepository.save(user);
    }

    public User findByEmail(String email) {
        return this.userRepository.findByEmail(email);
    }

    public User findByCpf(String cpf) {
        return this.userRepository.findByCpf(cpf);
    }

//    public User findByCpfAndActive(String cpf) {
//        return this.userRepository.findByCpfAndActive(cpf);
//    }

    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    public List<User> findAll(Sort sort) {
        return this.userRepository.findAll(sort);
    }

    public Page<User> findAll(Pageable pageable) {
        return this.userRepository.findAll(pageable);
    }

    public List<User> findAllById(Iterable<Long> iterable) {
        return this.userRepository.findAllById(iterable);
    }

    public long count() {
        return 0;
    }

    public void deleteById(Long aLong) {
        this.userRepository.deleteById(aLong);
    }

    public void delete(User user) {
        this.userRepository.delete(user);
    }

    public void deleteAll(Iterable<? extends User> iterable) {
        this.userRepository.deleteAll(iterable);
    }

    public void deleteAll() {
        this.userRepository.deleteAll();
    }

    public <S extends User> S save(S s) {
        return this.userRepository.save(s);
    }

    public <S extends User> List<S> saveAll(Iterable<S> iterable) {
        return this.userRepository.saveAll(iterable);
    }

    public Optional<User> findById(Long aLong) {
        return Optional.empty();
    }

    public boolean existsById(Long aLong) {
         if(this.userRepository.findById(aLong) != null){
             return true;
         }
         return false;
    }

    public void flush() {

    }

    public <S extends User> S saveAndFlush(S s) {
        return this.userRepository.saveAndFlush(s);
    }

    public void deleteInBatch(Iterable<User> iterable) {

    }

    public void deleteAllInBatch() {

    }

    public User getOne(Long aLong) {
        return this.userRepository.getOne(aLong);
    }

    public <S extends User> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    public <S extends User> List<S> findAll(Example<S> example) {
        return null;
    }

    public <S extends User> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    public <S extends User> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    public <S extends User> long count(Example<S> example) {
        return 0;
    }

    public <S extends User> boolean exists(Example<S> example) {
        return false;
    }
}
