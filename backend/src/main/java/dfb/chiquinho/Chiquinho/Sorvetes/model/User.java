package dfb.chiquinho.Chiquinho.Sorvetes.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Column(name = "fullName")
    @Length(min = 5, message = "*Seu nome deve ter pelo menos 5 characteres")
    @NotEmpty(message = "*Por favor, digite seu nome completo")
    private String fullName;

    @Column(name = "email")
    @Email(message = "*Por favor, digite um email válido")
    @NotEmpty(message = "*Por favor, digite seu email")
    private String email;

    @Column(name = "cpf")
    @Length(min = 14, max = 14, message = "*O CPF que você inseriu é inválido")
    @NotEmpty(message = "*Por favor, digite seu CPF. Ele será eu login")
    private String cpf;

    @Column(name = "phone")
    @NotEmpty(message = "*Por favor, digite seu celular. Ele será sua senha")
    private String phone;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "cash")
    private String cash;

    @Column(name = "instagram")
    private String instagram;

    @Column(name = "cep")
    @Length(min = 9, max = 9, message = "*O CEP que você inseriu é inválido")
    private String cep;

    @Column(name = "birthday")
    @Length(min = 10, max = 10, message = "*A data que você inseriu é inválido")
    private String birthday;

    @OneToMany(mappedBy = "user")
    private List<FiscalNote> fiscalNotes;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "user_voucher",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "voucher_id"))
    private Set<Voucher> vouchers;

}
