package dfb.chiquinho.Chiquinho.Sorvetes.service;


import dfb.chiquinho.Chiquinho.Sorvetes.model.FiscalNote;
import dfb.chiquinho.Chiquinho.Sorvetes.model.User;
import dfb.chiquinho.Chiquinho.Sorvetes.repository.FiscalNoteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FiscalNoteService{
    private final FiscalNoteRepository fiscalNoteRepository;

    @Autowired
    public FiscalNoteService(FiscalNoteRepository fiscalNoteRepository){
        this.fiscalNoteRepository = fiscalNoteRepository;
    }

    public FiscalNote saveFiscalNote(FiscalNote fiscalNote, User user){
        fiscalNote.setUser(user);
        fiscalNote.setActive(true); //desativado ao validar o cadastro
        return this.fiscalNoteRepository.save(fiscalNote);
    }
    public FiscalNote findFiscalNoteByCode(String code){
        return this.fiscalNoteRepository.findFiscalNoteByCode(code);
    }


//    public FiscalNote findFiscalNoteByActiveAndCode(String code) {
//        return this.fiscalNoteRepository.findFiscalNoteByActiveAndCode(code);
//    }

//    public List<FiscalNote> findFiscalNotesByUserAndActive(int id) {
//        return this.fiscalNoteRepository.findFiscalNotesByUserAndActive(id);
//    }


    public List<FiscalNote> findAll() {
        return null;
    }

    public List<FiscalNote> findAll(Sort sort) {
        return null;
    }

    public Page<FiscalNote> findAll(Pageable pageable) {
        return null;
    }

    public List<FiscalNote> findAllById(Iterable<Long> iterable) {
        return null;
    }

    public long count() {
        return 0;
    }

    public void deleteById(Long aLong) {

    }

    public void delete(FiscalNote fiscalNote) {

    }

    public void deleteAll(Iterable<? extends FiscalNote> iterable) {

    }

    public void deleteAll() {

    }

    public <S extends FiscalNote> S save(S s) {
        return null;
    }

    public <S extends FiscalNote> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    public Optional<FiscalNote> findById(Long aLong) {
        return Optional.empty();
    }

    public boolean existsById(Long aLong) {
        return false;
    }

    public void flush() {

    }

    public <S extends FiscalNote> S saveAndFlush(S s) {
        return null;
    }

    public void deleteInBatch(Iterable<FiscalNote> iterable) {

    }

    public void deleteAllInBatch() {

    }

    public FiscalNote getOne(Long aLong) {
        return null;
    }

    public <S extends FiscalNote> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    public <S extends FiscalNote> List<S> findAll(Example<S> example) {
        return null;
    }

    public <S extends FiscalNote> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    public <S extends FiscalNote> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    public <S extends FiscalNote> long count(Example<S> example) {
        return 0;
    }

    public <S extends FiscalNote> boolean exists(Example<S> example) {
        return false;
    }
}
