package dfb.chiquinho.Chiquinho.Sorvetes.repository;

import dfb.chiquinho.Chiquinho.Sorvetes.model.FiscalNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FiscalNoteRepository extends JpaRepository<FiscalNote, Long> {

        FiscalNote findFiscalNoteByCode(String code);
//        FiscalNote findFiscalNoteByActiveAndCode(String code);
//        List<FiscalNote> findFiscalNotesByUserAndActive(int id);
}
