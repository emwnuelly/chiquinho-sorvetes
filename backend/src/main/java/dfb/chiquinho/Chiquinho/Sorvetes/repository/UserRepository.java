package dfb.chiquinho.Chiquinho.Sorvetes.repository;

import dfb.chiquinho.Chiquinho.Sorvetes.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);
    User findByCpf(String cpf);
//    User findByCpfAndActive(String cpf);
}

