package dfb.chiquinho.Chiquinho.Sorvetes.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "fiscalNote")
public class FiscalNote {

    @Id
    @Column(name = "fiscalNote_code")
    @NotEmpty(message = "*É preciso registrar o código da nota fiscal")
    private int code;

    @Column(name = "date")
    private Date date;

    @Column(name = "totalValue")
    @NotEmpty(message = "*É preciso registrar o valor da nota fiscal")
    private double totalValue;

    @Column(name = "active")
    private boolean active;

    @Column(name = "image")
    @NotEmpty(message = "*É preciso registrar a imagem da nota fiscal")
    private boolean image;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="user_id")
    private User user;

//    @ManyToMany(cascade = CascadeType.MERGE)
//    @JoinTable(name = "fiscalNote_product",
//            joinColumns = @JoinColumn(name = "fiscalNote_code"),
//            inverseJoinColumns = @JoinColumn(name = "product_id"))
//    private Set<Product> products;
}
