# Gostinho de Felicidade :shaved_ice:
**_Por Alessio Miranda, Emanuelly Carvalho e Iane Santos_**

A sorveteria **Chiquinho Sorvetes** está com uma oferta imperdível na qual você ganha um cashback de 10% em todas as compras, além de cashbacks diferenciados em produtos especiais da promoção. O dinheiro recebido de volta fica armazenado em forma de saldo positivo na conta do cliente para que ele possa utilizar em compras futuras. 
<br><br>
Para gerenciar o funcionamento dessa oferta, foi implementado um sistema no qual os operadores de caixa precisam registrar a compra de cada cliente para que seja calculado o valor do cashback e armazenado o saldo na conta do cliente. O problema é que o operador também precisa fazer esse processo no sistema regular da sorveteria para que a venda fique registrada, fazendo com que ele realize procedimentos semelhantes duas vezes para cada cliente. Isso deixa o fluxo de caixa mais lento, uma vez que o tempo gasto com cada cliente torna-se o dobro.
<br><br>
Para resolver esse problema, foi pensado um aplicativo no qual o próprio cliente faz o controle do seu cashback, deixando poucas etapas para o operador de caixa, que ganha em tempo. 
<br><br>
Ao realizar uma venda, o operador de caixa vai perguntar se o cliente tem a intenção de acumular o cashback. Para isso, é necessário o uso do aplicativo que o cliente vai baixar em seu aparelho móvel. No sistema do cliente, ele se cadastra e/ou acessa sua conta e tem acesso ao seu saldo e a diversas funcionalidades, sendo uma delas o cadastro da nota fiscal. Caso haja o interesse em cadastrá-la, o operador vai escrever uma espécie de código na nota fiscal que vai ser solicitada no cadastro do documento, apenas para dificultar a fraude, junta ao código, à data e ao valor impresso nele. Se o cliente tiver comprado algum dos produtos que tenha cashback especial, ele seleciona esses produtos no momento do cadastro.
<br><br>
No final do dia, os operadores de caixa conferem a relação da nota fiscal com os produtos cadastrados com o cashback - que o próprio sistema calcula, bastando um clique para validar o cadastro do documento e a adição do saldo na conta que efetuou o cadastro.
<br><br>
Para usar o saldo, o cliente gera um QRCode no aplicativo que vai ser escaneado pelo dispositivo do operador de caixa, que vai, a partir dele, visualizar o nome do cliente e a quantia disponível, debitando, em seguida, o valor do total da compra.
Assim, os operadores de caixa terão menos funções relacionadas à promoção para realizar no momento da compra, aumentando o fluxo da loja e evitando a espera, as filas, a sobrecarga do funcionário e o descontentamento do cliente.

