import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {CodePageRoutingModule} from './code-routing.module';
import {CodePage} from './code.page';
import {QRCodeModule} from 'angularx-qrcode';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QRCodeModule,
    CodePageRoutingModule
  ],
  declarations: [CodePage]
})
export class CodePageModule {}
