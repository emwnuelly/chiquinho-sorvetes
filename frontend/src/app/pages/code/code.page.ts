import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-code',
  templateUrl: './code.page.html',
  styleUrls: ['./code.page.scss'],
})
export class CodePage implements OnInit {
  title = 'app';
  elementType = 'url';
  value: string;

  constructor(
    private authService: AuthService
  ) {
  }

  ngOnInit() {
    this.value = `${environment.API_URL}/urlparapegardadoscliente${this.authService.user.id}`;
  }

}
