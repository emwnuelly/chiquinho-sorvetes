import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(
    private http: HttpClient
  ) {
  }

  public getVouchers() {
    return this.http.get(`${environment.API_URL}api/account/${environment.DOMAIN_KEY}/vouchers`);
  }

  public getTransacoes() {
    return this.http.get<any[]>(`${environment.API_URL}api/account/${environment.DOMAIN_KEY}/transacoes`)
      .pipe(map((transacoes: any[]) => {
        transacoes.forEach(transacao => {
          transacao.dtTransacao = moment(transacao.dtTransacao);
        });
        return transacoes;
      }));
  }

  public getJogador(login) {
    return this.http.get(`${environment.API_URL}api/jogos/${environment.DOMAIN_KEY}/jogadores/${login}`);
  }
}
