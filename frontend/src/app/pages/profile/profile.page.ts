import {Component, OnInit} from '@angular/core';
import {ProfileService} from './profile.service';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  public selectedSegment = 'vouchers';
  vouchers: any[] = [];
  transacoes: any[] = [];
  jogador: any;

  constructor(
    private profileService: ProfileService,
    public authService: AuthService
  ) {
  }

  async segmentChanged(ev) {
    this.selectedSegment = ev.target.value;
  }

  ionViewWillEnter() {
    this.profileService.getJogador(this.authService.user.login.replace(/\./g, '').replace('-', '')).subscribe((jogador: any) => {
      this.jogador = jogador;
    });
    this.profileService.getVouchers().subscribe((vouchers: any[]) => {
      this.vouchers = vouchers || [];
    });
    this.profileService.getTransacoes().subscribe((compras: any[]) => {
      this.transacoes = compras || [];
    });
  }

  ngOnInit() {
  }

}
