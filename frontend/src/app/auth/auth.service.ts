import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {SessionStorageService} from 'ngx-webstorage';
import {map, shareReplay, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {SharedService} from '../shared.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$: Observable<any | null>;
  user: any;

  constructor(
    private http: HttpClient,
    private sessionStorage: SessionStorageService,
    private sharedService: SharedService
  ) {
  }

  register(user) {
    return this.http.post(`${environment.API_URL}api/register`, user);
  }

  update(user) {
    return this.http.post(`${environment.API_URL}api/account`, user);
  }

  updateCEP(login, endereco) {
    return this.http.put(`${environment.API_URL}api/jogadores/${login}/updateCEP`, endereco);
  }

  getEnderecoCEP(cep: string){
    return this.http.get(`https://viacep.com.br/ws/${cep}/json/`);
  }

  getAccount() {
    if (!this.user$ || !this.user) {
      this.user$ = this.http.get(`${environment.API_URL}api/account`).pipe(tap(user => {
        this.user = user;
      }), shareReplay(1));
    } else {
      console.warn('poisé');
    }
    return this.user$;
  }

  login(login) {
    return this.user$ = this.http.post(`${environment.API_URL}api/authenticate`, login).pipe(map((token: any) => {
      if (token?.id_token) {
        this.sessionStorage.store('id_token', token.id_token);
        return true;
      } else {
        return false;
      }
    }));
  }

  logout() {
    this.sessionStorage.clear('id_token');
    this.user$ = null;
    this.user = null;
    this.sharedService.navigateForward('/home');
  }
}
