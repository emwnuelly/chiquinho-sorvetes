import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SessionStorageService} from 'ngx-webstorage';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(
    private sessionStorage: SessionStorageService
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes('influenzer')) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer  ${this.sessionStorage.retrieve('id_token')}`
        }
      });
    }
    return next.handle(req);
  }
}
