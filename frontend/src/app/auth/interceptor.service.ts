import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AuthService} from './auth.service';
import {catchError, map} from 'rxjs/operators';
import {SharedService} from '../shared.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements CanActivate {

  constructor(
    private authService: AuthService,
    private sharedService: SharedService
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authService.getAccount().pipe(
      catchError(() => {
        this.sharedService.navigateBack('/login');
        return of(false);
      }), map((user: any) => {
        if (!user) {
          this.sharedService.navigateBack('/login');
          return false;
        } else {
          console.warn('oiii');
          return true;
        }
      }));
  }
}
