import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../auth.service';
import {SharedService} from '../../shared.service';
import {environment} from '../../../environments/environment';
import {ProfileService} from '../../pages/profile/profile.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  editForm: FormGroup;
  editFormCEP: FormGroup;

  constructor(
    private authService: AuthService,
    private sharedService: SharedService,
    private profileService: ProfileService
  ) {
    this.editForm = new FormGroup({
      cpf: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      fullName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
    });

    this.editFormCEP = new FormGroup({
      bairro: new FormControl('', [Validators.required]),
      rua: new FormControl('', [Validators.required]),
      uf: new FormControl('', [Validators.required]),
      cidade: new FormControl('', [Validators.required]),
      cep: new FormControl('')
    });
  }

  updateForm() {
    let fullName = this.authService.user.firstName + ' ' + this.authService.user.lastName;
    if (!fullName) {
      fullName = '';
    }
    this.editForm.get('cpf').setValue(this.authService.user.login);
    this.editForm.get('fullName').setValue(fullName);
    this.editForm.get('email').setValue(this.authService.user.email);
    this.editForm.get('cpf').setValue(this.authService.user.login);
  }

  updateFormCEP(endereco) {
    if (endereco) {
      this.editFormCEP.get('rua').setValue(endereco?.rua);
      this.editFormCEP.get('uf').setValue(endereco?.uf);
      this.editFormCEP.get('bairro').setValue(endereco?.bairro);
      this.editFormCEP.get('cidade').setValue(endereco?.cidade);
      this.editFormCEP.get('cep').setValue(endereco?.cep);
    }
  }

  update() {
    if (this.editForm.valid) {
      this.authService.update(this.getForm()).subscribe(() => {
        this.sharedService.presentToast('Editado com sucesso');
      });
    }
  }

  updateCEP() {
    if (this.editFormCEP.valid) {
      this.authService.updateCEP(this.authService.user.login.replace(/\./g, '').replace('-', ''), this.getFormCEP()).subscribe(() => {
        this.sharedService.presentToast('Endereço editado com sucesso');
      });
    }
  }

  getEnderecoCEP() {
    if (this.editFormCEP.get('cep').value) {
      this.authService.getEnderecoCEP(this.editFormCEP.get('cep').value).subscribe((endereco: any) => {
        this.editFormCEP.get('rua').setValue(endereco?.logradouro);
        this.editFormCEP.get('bairro').setValue(endereco?.bairro);
        this.editFormCEP.get('cidade').setValue(endereco?.localidade);
        this.editFormCEP.get('uf').setValue(endereco?.uf);
      });
    }
  }

  ionViewWillEnter() {
    this.profileService.getJogador(this.authService.user.login.replace(/\./g, '').replace('-', '')).subscribe((jogador: any) => {
      this.updateFormCEP(jogador?.pessoa);
    });
    this.updateForm();

  }

  ngOnInit() {
  }

  getForm() {
    const fragmentedName: any[] = String(this.editForm.get('fullName').value).split(' ');
    const user: any = {};
    user.firstName = fragmentedName[0];
    fragmentedName.shift();
    user.lastName = fragmentedName.toString().replace(/,/g, ' ');
    user.login = this.editForm.get('cpf').value;
    user.email = this.editForm.get('email').value;
    user.password = this.editForm.get('password').value;
    return user;
  }

  getFormCEP() {
    const endereco: any = {};
    endereco.rua = this.editFormCEP.get('rua').value;
    endereco.cidade = this.editFormCEP.get('cidade').value;
    endereco.uf = this.editFormCEP.get('uf').value;
    endereco.bairro = this.editFormCEP.get('bairro').value;
    endereco.jogador = this.authService.user.login.replace(/\./g, '').replace('-', '');
    endereco.domainKey = environment.DOMAIN_KEY;
    return endereco;
  }

}
