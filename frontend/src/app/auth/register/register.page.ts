import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SharedService} from '../../shared.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;

  constructor(
    private authService: AuthService,
    private sharedService: SharedService
  ) {
    this.registerForm = new FormGroup({
      cpf: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      confirmPassword: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      fullName: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.registerForm.get('cpf').setValue('');
    this.registerForm.get('password').setValue('');
    this.registerForm.get('confirmPassword').setValue('');
    this.registerForm.get('email').setValue('');
    this.registerForm.get('fullName').setValue('');
    this.authService.getAccount().subscribe((user) => {
      if (user) {
        this.sharedService.navigateForward('/profile');
      }
    });
  }

  register() {
    if (this.registerForm.valid) {
      if (this.registerForm.get('password').value === this.registerForm.get('confirmPassword').value) {
        const user = this.getForm();
        this.authService.register(user).subscribe(() => {
          this.sharedService.presentToast('Cadastrado com sucesso');
          this.authService.login(this.getFormlogin(user)).subscribe((ok: boolean) => {
            if (ok) {
              this.sharedService.presentToast('Logado com sucesso');
              this.sharedService.navigateForward('/profile');
            } else {
              this.sharedService.presentToast('Erro ao realizar login direto');
            }
          }, () => {
            this.sharedService.presentToast('Erro ao realizar login direto');
          });
          // this.sharedService.navigateForward('/profile');
        }, error => {
          if (error.error.errorKey === 'userexists') {
            this.sharedService.presentToast('Esse usuário já existe');
          }
          console.warn(error);
        });
      } else {
        this.sharedService.presentToast('As senhas são diferentes');
      }
    }
  }

  getForm() {
    const fragmentedName: any[] = String(this.registerForm.get('fullName').value).split(' ');
    const user: any = {};
    user.firstName = fragmentedName[0];
    fragmentedName.shift();
    user.lastName = fragmentedName.toString().replace(',', ' ');
    user.login = this.registerForm.get('cpf').value;
    user.email = this.registerForm.get('email').value;
    user.password = this.registerForm.get('password').value;
    return user;
  }

  getFormlogin(user1: any) {
    const user: any = {};
    user.username = user1?.login;
    user.password = user1?.password;
    user.rememberMe = true;
    return user;
  }
}
