import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../auth.service';
import {SharedService} from '../../shared.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;

  constructor(
    private authService: AuthService,
    private sharedService: SharedService
  ) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.loginForm.get('username').setValue('');
    this.loginForm.get('password').setValue('');
    this.authService.getAccount().subscribe((user) => {
      if (user) {
        this.sharedService.navigateForward('/profile');
      }
    });
  }

  login() {
    if (this.loginForm.valid) {
      this.authService.login(this.getForm()).subscribe((ok: boolean) => {
        if (ok) {
          this.sharedService.presentToast('Logado com sucesso');
          this.sharedService.navigateForward('/profile');
        } else {
          this.sharedService.presentToast('Erro ao logar');
        }
      }, (error) => {
        if (error.error.detail === 'Usuário inexistente ou senha inválida') {
          this.sharedService.presentToast('Usuário inexistente ou senha inválida');
        } else {
          this.sharedService.presentToast('Erro ao logar');
        }
      });
    }
  }

  getForm() {
    const user: any = {};
    user.username = this.loginForm.get('username').value;
    user.password = this.loginForm.get('password').value;
    user.rememberMe = true;
    return user;
  }

}
