export interface IUser {
  login: string;
  password: string;
  email: string;
}

export class User implements IUser {
  email: string;
  login: string;
  password: string;

}
