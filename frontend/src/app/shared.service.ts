import {Injectable} from '@angular/core';
import {NavController, ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(
    public toastController: ToastController,
    private navController: NavController
  ) {
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  public navigateBack(url: string) {
    this.navController.navigateBack(url);
  }

  public navigateForward(url: string) {
    this.navController.navigateForward(url);
  }
}
